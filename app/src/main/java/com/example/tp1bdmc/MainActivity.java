package com.example.tp1bdmc;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private FirebaseUser user;
    private DatabaseReference reference;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        userId = user.getUid();

        final TextView mUsername, mEmail,mPw1,mPhoneNumber;
        mUsername = findViewById(R.id.txtUsername);
        mEmail = findViewById(R.id.txtEmail);
        mPw1 = findViewById(R.id.txtPassword);
        mPhoneNumber = findViewById(R.id.txtPhoneNumber);
        final EditText username, email,pw1,phoneNumber;
        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        pw1 = findViewById(R.id.pw1);
        phoneNumber = findViewById(R.id.phoneNumber);
        Button btnUpdate, btnDelete;
        btnUpdate = findViewById(R.id.btnUpdate);;
        btnDelete = findViewById(R.id.btnDelete);
        HashMap<String,Object> hashMap = new HashMap<>();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(email.getText().toString())) {
                    reference.child(userId).child("email").setValue(email.getText().toString());
                    user.updateEmail(email.getText().toString());
                    mEmail.setText("Email: " + email.getText().toString());
                }
                if (!TextUtils.isEmpty(pw1.getText().toString())) {
                    reference.child(userId).child("password").setValue(pw1.getText().toString());
                    user.updatePassword(pw1.getText().toString());
                    mPw1.setText("Password: " + pw1.getText().toString());
                }
                if (!TextUtils.isEmpty(username.getText().toString())) {
                    reference.child(userId).child("username").setValue(username.getText().toString());
                    mUsername.setText("Username: " + username.getText().toString());
                }
                if (!TextUtils.isEmpty(phoneNumber.getText().toString())) {
                    reference.child(userId).child("phoneNumber").setValue(phoneNumber.getText().toString());
                    mPhoneNumber.setText("Phone Number: " + phoneNumber.getText().toString());
                }

                Toast.makeText(MainActivity.this, "update done successfully!", Toast.LENGTH_SHORT);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reference.child(userId).removeValue();
                user.delete();
                startActivity(new Intent(getApplicationContext(), Login.class));
                Toast.makeText(MainActivity.this, "Account deleted successfully!", Toast.LENGTH_SHORT);

                finish();
            }
        });




        reference.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User userProfile = snapshot.getValue(User.class);

                if (userProfile != null){
                    String username = userProfile.username;
                    String password = userProfile.password;
                    String email = userProfile.email;
                    String phoneNumber = userProfile.phoneNumber;

                    mEmail.setText("Email: " + email);
                    mUsername.setText("username: " + username);
                    mPw1.setText("password: " + password);
                    mPhoneNumber.setText("Phone Number: " + phoneNumber);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this, "Something wrong happened!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void logout(View view) {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getApplicationContext(), Login.class));
        finish();
    }
}