package com.example.tp1bdmc;

public class User {

    public String username;
    public String password;
    public String email;
    public String phoneNumber;
    public User(){}
    public User(String username,String password,String email,String phoneNumber){
        this.email = email;
        this.password = password;
        this.username = username;
        this.phoneNumber = phoneNumber;
    }
}