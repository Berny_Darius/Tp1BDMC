package com.example.tp1bdmc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class Register extends AppCompatActivity implements View.OnClickListener{
    private EditText mUsername, mEmail,mPw1,mPhoneNumber ;
    private Button mRegisterBtn;
    private FirebaseAuth fAuth;

    //    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        fAuth = FirebaseAuth.getInstance();

        mRegisterBtn = (Button) findViewById(R.id.btnRegister);
        mRegisterBtn.setOnClickListener((View.OnClickListener) this);




        mUsername = findViewById(R.id.username);
        mEmail = findViewById(R.id.email);
        mPw1 = findViewById(R.id.pw1);
        mPhoneNumber = findViewById(R.id.phoneNumber);
        Button btnloginRedirect;
        btnloginRedirect = findViewById(R.id.btnloginRedirect);

        btnloginRedirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Login.class));
            }
        });

    }
        public void onClick(View view){
                btnRegister();
        }

    private void btnRegister() {
        String email = mEmail.getText().toString().trim();
        String username = mUsername.getText().toString().trim();
        String pw = mPw1.getText().toString().trim();
        String phoneNumber = mPhoneNumber.getText().toString().trim();

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmail.setError("Doit avoir email ");
            mEmail.requestFocus();
        return;
        }
        if (username.isEmpty()) {
            mUsername.setError("Doit avoir username ");
            mUsername.requestFocus();
            return;
        }
        if (pw.isEmpty()) {
            mPw1.setError("Doit avoir password ");
            mPw1.requestFocus();
            return;
        }
        if (phoneNumber.isEmpty()) {
            mPhoneNumber.setError("Doit avoir phone number ");
            mPhoneNumber.requestFocus();
            return;
        }

        fAuth.createUserWithEmailAndPassword(email,pw)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                             User user = new User(username,pw,email,phoneNumber);

                            FirebaseDatabase.getInstance().getReference("Users")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        Toast.makeText(Register.this,"register successful",Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                    } else {
                                        Toast.makeText(Register.this,"Failed to register",Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                        }
                    }
                });

    }
}